package com.example.locationservicestest;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;
import android.widget.ToggleButton;

public class LocationService extends Service {
	
	
	WakeLock wakeLock;
	private LocationManager locationManager;
	//private LocationListener myLocationListener;

	double latitude;
	double longitude;
	
	Calendar c = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyy-MM-dd HH:mm:ss ");
	String strDate = sdf.format(c.getTime());

	 
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		PowerManager pm = (PowerManager) getSystemService(this.POWER_SERVICE);

		wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");
		
		

		// Toast.makeText(getApplicationContext(), "Service Created",
		// Toast.LENGTH_SHORT).show();

		Log.e("Google", "Service Created");

	}



	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
	          
	            // register location updates, not gonna code it you know

	        
		super.onStart(intent, startId);
	wakeLock.acquire();
	
		final LocationManager	locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	//	myLocationListener = new MyLocationListener();
		try{

			locationManager.requestLocationUpdates(
			        LocationManager.NETWORK_PROVIDER,
			        0,
			        5,
			        listener);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}

		//locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 4000, 0, listener);
		//locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
		//locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		Log.e("Service:", "Service Started");
		
		
		

	
}
	
	

	

	private LocationListener listener = new LocationListener() {
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			
			Log.e("Google", "Location Changed");

			if (location == null)
				return;

			if (isConnectingToInternet(getApplicationContext())) {
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();

				try {
					Log.e("Date/Time", strDate + "");
					Log.e("latitude", location.getLatitude() + "");
					Log.e("longitude", location.getLongitude() + "");

					jsonObject.put("longitude", location.getLongitude() );
					jsonObject.put("latitude", location.getLatitude() );
					jsonObject.put("DateTime", strDate);
					
					jsonArray.put(jsonObject);

					Log.e("request", jsonArray.toString());
					
					

					new LocationWebService().execute(new String[] {
							Constants.TRACK_URL, jsonArray.toString() });
					Thread.sleep(120000);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			
		}
		
	};

		private boolean isConnectingToInternet(Context _context) {
			// TODO Auto-generated method stub
			ConnectivityManager connectivity = (ConnectivityManager) _context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null)
					for (int i = 0; i < info.length; i++)
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}

			}

			return false;
		}
		
		
	
	
}


	
	
